The project is created using create-react-app.

I have completed the test in approximately 7 hours as per the senior front end developer guidelines using reactjs as primary framework of javascript.

Plus, I have created a backend api for fetching the data using express framework and nodejs.

I worked on creating a service folder containing data.js file. I worked around random generated data according to three different types of sports.

I got stuck on the radio button checked attribute saving into the state.

Step by step:-

1. First clone the repository into your local repository.

2. Open the project in any of the IDE and open the integrated terminal or using git bash.

3. In the root folder, type command npm install which will install all the dependencies required.

4. Open the server folder and run npm install also for the backend service. Optional (Start the backend by going into the server folder and typing command in terminal npm start).

5. For starting the react app, in the root folder, go to the integrated terminal and type npm start and it will automatically open a browser or you can type localhost:3000 in the url bar.
