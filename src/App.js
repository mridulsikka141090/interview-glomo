import React, { Component } from "react";
import "./App.css";
import { getAllData } from "./services/Data";
import Poll from "./components/Poll";
import football from "./images/football.png";
import tennis from "./images/tennis.jpg";
import snooker from "./images/snooker.jpg";

class App extends Component {
  constructor() {
    super();

    this.state = {
      data: [],
      selected: "",
      disabled: true
    };
  }

  componentWillMount = () => {
    this.setState({
      data: getAllData()
    });
  };

  componentDidMount = () => {
    const { sport } = this.state.data[0];
    if (sport === "FOOTBALL") {
      this.setState({
        selected: localStorage.getItem("FOOTBALL")
      });
    } else if (sport === "TENNIS") {
      this.setState({
        selected: localStorage.getItem("TENNIS")
      });
    } else {
      this.setState({
        selected: localStorage.getItem("SNOOKER")
      });
    }
  };

  handleImages = () => {
    const { sport } = this.state.data[0];
    if (sport === "FOOTBALL") {
      return football;
    } else if (sport === "TENNIS") {
      return tennis;
    } else {
      return snooker;
    }
  };

  handleChange = e => {
    this.setState({
      selected: e.target.value,
      disabled: false
    });
  };

  handleClearLocalStorage = () => {
    localStorage.clear();
    window.location.reload();
  };

  handleSubmit = event => {
    console.log(this.state.disabled);
    localStorage.setItem(this.state.data[0].sport, this.state.selected);
  };

  handleData = item => {
    const { sport } = this.state.data[0];
    if (sport === "FOOTBALL") {
      return item.country;
    } else if (sport === "TENNIS") {
      return item.awayName;
    } else {
      return item.homeName;
    }
  };

  render() {
    console.log(this.state.selected);
    return (
      <div className="app">
        <div className="logo">
          <img src={this.handleImages()} />
        </div>
        <Poll
          items={this.state.data}
          selected={this.state.selected}
          disabled={this.state.disabled}
          handleData={this.handleData}
          submitValue={this.handleSubmit}
          handleChange={this.handleChange}
          handleClearLocalStorage={this.handleClearLocalStorage}
        />
      </div>
    );
  }
}

export default App;
