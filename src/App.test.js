import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Poll from "./components/Poll";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("Poll Component without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Poll />, div);
  ReactDOM.unmountComponentAtNode(div);
});
