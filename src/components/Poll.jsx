import React from "react";

const Poll = props => {
  return (
    <div className="poll">
      <form onSubmit={props.submitValue}>
        <h3>Who do you think will win the game?</h3>
        <h3>{props.items[0].sport}</h3>
        <ul>
          {props.items.map((item, i) => {
            return (
              <li key={i}>
                <input
                  name="poll_value"
                  type="radio"
                  value={i}
                  onChange={props.handleChange}
                  checked={props.selected == i}
                />
                {props.handleData(item)}
              </li>
            );
          })}
          <li>
            <input
              name="poll_value"
              type="radio"
              value="Draw"
              onChange={props.handleChange}
              checked={props.selected == "Draw"}
            />
            DRAW
          </li>
        </ul>
        <input
          type="submit"
          value="Submit"
          disabled={props.disabled ? "disabled" : ""}
        />
      </form>
      <button onClick={props.handleClearLocalStorage}>
        Clear Local Storage
      </button>
    </div>
  );
};

export default Poll;
